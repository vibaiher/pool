Pool
====

## Resources

- [Pooling - Michael Kircher, Prashant Jain](http://www.kircher-schwanninger.de/michael/publications/Pooling.pdf)
- [Object Pool - Game Programming Patterns](http://gameprogrammingpatterns.com/object-pool.html)
- [Resource Pool - Martin Fowler](https://www.martinfowler.com/bliki/ResourcePool.html)
- [Pattern-Oriented Software Architecture Volume 3: Patterns for Resource Management](http://posa3.org)
