require 'client'

class Pool
  def self.get
    @instance ||= new

    @instance
  end

  def self.destroy
    @instance = nil
  end

  private_class_method :new

  def initialize
    setup
  end

  def get_client
    next_available
  end

  def clients
    number_of_clients
  end

  private

  MAX_AVAILABILITY = 5

  def build_client
    Client.new
  end

  def setup
    @clients = []
  end

  def next_available
    ensure_availability

    @clients.last
  end

  def ensure_availability
    return if limit_reached?

    client = build_client
    @clients.push(client)
  end

  def limit_reached?
    @clients.count >= MAX_AVAILABILITY
  end

  def number_of_clients
    @clients.size
  end
end
