require 'pool'
require 'client'

describe 'Pool' do
  describe 'as a Singleton' do
    it 'produces always same instance' do
      one_instance = Pool.get()
      another_instance = Pool.get()

      expect(one_instance).to eq(another_instance)
    end

    it 'has its constructor protected' do
      expect {
        Pool.new
      }.to raise_error(NoMethodError)
    end
  end

  describe 'as a Producer' do
    it 'produces a Client' do
      pool = Pool.get()

      client = pool.get_client()

      expect(client).to be_kind_of(Client)
    end
  end

  describe 'as a dynamic Pool' do
    it 'starts without clients' do
      Pool.destroy()
      pool = Pool.get()

      none = 0
      expect(pool.clients()).to eq(none)
    end

    it 'controls the number of clients produced' do
      pool = Pool.get()
      initial = pool.clients()
      some = 3

      some.times { pool.get_client() }

      expect(pool.clients()).to eq(initial + some)
    end
  end

  describe 'as a limited Pool' do
    it 'has a maximum client availability' do
      pool = Pool.get()
      maximum_default_availability = 5
      exceeded_number = maximum_default_availability + 1

      exceeded_number.times { pool.get_client() }

      expect(pool.clients()).to eq(maximum_default_availability)
    end
  end
end
